//
//  ViewController.swift
//  Asset_Telematics_task
//
//  Created by Ruby's Mac on 13/07/21.
//

import UIKit
import iOSDropDown
import AVFoundation


class ViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate,UICollectionViewDataSource,UICollectionViewDelegate{
   

    @IBOutlet var IMEITextField: UITextField!
    @IBOutlet var vehicleMakeDropDownTF: DropDown!
    @IBOutlet var vehicleModelDropDownTF: DropDown!
    @IBOutlet var manufactureYearDropDownTF: DropDown!
    @IBOutlet var fuelTypeDropDownTF: DropDown!
    @IBOutlet var capacityDropDownTF: DropDown!
    
    @IBOutlet var ownerButton: UIButton!
    @IBOutlet var contractorButton: UIButton!
    @IBOutlet var vehicleCollectionView: UICollectionView!
    @IBOutlet var collectionViewHieght: NSLayoutConstraint!
    @IBOutlet var moreVehicles: UIButton!
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    let urlString = "http://34.70.239.163/jhsmobileapi/mobile/configure/v1/configvalues"
    
   

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        vehicleData.enter()
        callPostWebservice(urlString)
        collectionViewHieght.constant = 70
        vehicleCollectionView.isScrollEnabled = false
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
         
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
         
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession?.addInput(input)
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        ownerButton.addTarget(self, action: #selector(radioButtonSelected), for: .touchUpInside)
        contractorButton.addTarget(self, action: #selector(radioButtonSelected), for: .touchUpInside)

        vehicleData.notify(queue: .main){
            if fuelTypeArray.count > 0{
                self.fuelTypeDropDownTF.optionArray =  fuelTypeArray
                self.fuelTypeDropDownTF.didSelect{(selectedText , index ,id) in
                self.fuelTypeDropDownTF.text = "\(selectedText)"
                }
            }
            if yearDataArray.count > 0{
                self.manufactureYearDropDownTF.optionArray =  yearDataArray
                self.manufactureYearDropDownTF.didSelect{(selectedText , index ,id) in
                self.manufactureYearDropDownTF.text = "\(selectedText)"
                }
            }
            
            if vehicleCapacityArray.count > 0{
                self.capacityDropDownTF.optionArray =  vehicleCapacityArray
                self.capacityDropDownTF.didSelect{(selectedText , index ,id) in
                self.capacityDropDownTF.text = "\(selectedText)"
                }
            }
            
            if vehicleTypeArray.count > 0{
                self.vehicleModelDropDownTF.optionArray =  vehicleTypeArray
                self.vehicleModelDropDownTF.didSelect{(selectedText , index ,id) in
                self.vehicleModelDropDownTF.text = "\(selectedText)"
                }
            }
            
            if vehicleMakeArray.count > 0{
                self.vehicleMakeDropDownTF.optionArray =  vehicleMakeArray
                self.vehicleMakeDropDownTF.didSelect{(selectedText , index ,id) in
                self.vehicleMakeDropDownTF.text = "\(selectedText)"
                }
            }
            self.vehicleCollectionView.reloadData()
        }
        
    }
    
    @IBAction func IMEIQRScrannerAction(_ sender: Any) {
        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
        captureSession?.startRunning()
       
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
//            messageLabel.text = "No QR code is detected"
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
//                messageLabel.text = metadataObj.stringValue
            }
        }
        }
    
    
    @objc func radioButtonSelected(sender:UIButton){
         sender.isSelected = !sender.isSelected
         if sender.isSelected{
            sender.isSelected = true
            
         }else{
            sender.isSelected = false
         }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return vehicleTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = vehicleCollectionView.dequeueReusableCell(
              withReuseIdentifier: "VechicleCollectionViewCell",
              for: indexPath) as! VechicleCollectionViewCell
        cell.vehicleTypeLabel.text = vehicleTypeArray[indexPath.row]
        return cell
    }
    
    @IBAction func moreVehicleTypeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            collectionViewHieght.constant = 70
            vehicleCollectionView.isScrollEnabled = false

        }else{
            collectionViewHieght.constant = 120
            vehicleCollectionView.isScrollEnabled = true

        }
        
    }
    
}

