//
//  AddVehicleViewModel.swift
//  Asset_Telematics_task
//
//  Created by Ruby's Mac on 13/07/21.
//

import Foundation

let urlString = "http://34.70.239.163/jhsmobileapi/mobile/configure/v1/task"
var fuelTypeArray = [String]()
var yearDataArray = [String]()
var vehicleMakeArray = [String]()
var vehicleTypeArray = [String]()
var vehicleCapacityArray = [String]()
let vehicleData = DispatchGroup()


func callPostWebservice(_ urlStr: String) {
    let url1 = URL(string: urlString)
    var request = URLRequest(url: url1!)
    let tokenId = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Ijg4NGExNDE0LTdjMjEtMTFlYi04ZTJiLTAwNTA1NmFmMjY5YiIsImlhdCI6MTYyNTk5MDAxNywiZXhwIjoxNjI2MTYyODE3fQ.3zhlbC-1HrDrL5oRu4o7J1pV05ViJGiarAGlK_h_MHI"
    let parameterDictionary = ["clientid" : 11,"mno" : "9889897789","enterprise_code" : 1007,"passcode": 3476] as [String : Any]

    request.httpMethod = "POST"
    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("Basic \(tokenId)", forHTTPHeaderField: "Authorization")
    request.timeoutInterval = 20000.0
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameterDictionary, options: []) else {
        return
    }
    request.httpBody = httpBody
    
    let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard let data = data else { return }
            do {
                print(data)
                do {
                    //create json object from data
                    let parsedData = try JSONSerialization.jsonObject(with: data) as! [String:Any]
                    let fuelType = parsedData["fuel_type"] as? [Any]
                    for i in 0..<fuelType!.count{
                        let text = fuelType![i] as! NSDictionary
                        if let fuel = text["text"]{
                        fuelTypeArray.append(fuel as! String)
                        }
                    }
                    
                    let yearArray = parsedData["manufacture_year"] as? [Any]
                    for i in 0..<yearArray!.count{
                        let text = yearArray![i] as! NSDictionary
                        if let year = text["text"]{
                            yearDataArray.append(year as! String)
                        }
                    }
                    
                    let vehicle = parsedData["vehicle_make"] as? [Any]
                    for i in 0..<vehicle!.count{
                        let text = vehicle![i] as! NSDictionary
                        if let vehicleMake = text["text"]{
                            vehicleMakeArray.append(vehicleMake as! String)
                        }
                    }
                    
                    let vehicletype = parsedData["vehicle_type"] as? [Any]
                    for i in 0..<vehicletype!.count{
                        let text = vehicletype![i] as! NSDictionary
                        if let vehicleMake = text["text"]{
                            vehicleTypeArray.append(vehicleMake as! String)
                        }
                    }
                    let vehiclecapcity = parsedData["vehicle_capacity"] as? [Any]
                    for i in 0..<vehiclecapcity!.count{
                        let text = vehiclecapcity![i] as! NSDictionary
                        if let vehicleMake = text["text"]{
                            vehicleCapacityArray.append(vehicleMake as! String)
                        }
                    }
                    
  
                    vehicleData.leave()

                } catch let error {
                    print(error.localizedDescription)
                }
            } catch let err {
                print("Err", err)
            }
        }.resume()
    
    
}

